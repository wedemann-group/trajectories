{
  "paper": {
    "title": "Nucleosome spacing controls chromatin spatial structure and accessibility",
    "authors": [
      "Tilo, Zuelske",
      "Aymen, Attou",
      "David, Hörl",
      "Laurens, Groß",
      "Hartmann, Harz",
      "Gero, Wedemann"
    ],
    "doi": "undefined in the moment"
  },
  "files": [
    {
      "name": "EquiDistant_320nm_304µM",
      "path": "PaperDensity/Regular_320_SAMPLE.zip"
    },
    {
      "name": "EquiDistant_390nm_168µM",
      "path": "PaperDensity/Regular_390_SAMPLE.zip"
    },{
      "name": "EquiDistant_415nm_145µM",
      "path": "PaperDensity/Regular_415_SAMPLE.zip"
    },
    {
      "name": "EquiDistant_440nm_117µM",
      "path": "PaperDensity/Regular_440_SAMPLE.zip"
    },{
      "name": "Random_390nm_168µM",
      "path": "PaperDensity/Irregular_390_SAMPLE.zip"
    },
    {
      "name": "Random_415nm_145µM",
      "path": "PaperDensity/Irregular_415_SAMPLE.zip"
    },{
      "name": "Random_440nm_117µM",
      "path": "PaperDensity/Irregular_440_SAMPLE.zip"
    },
    {
      "name": "Inactive_K562_415nm_145µM",
      "path": "PaperDensity/Inactive_SAMPLE.zip"
    }
  ]
}
